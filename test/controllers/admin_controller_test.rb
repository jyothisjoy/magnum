require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get newincharge" do
    get :newincharge
    assert_response :success
  end

  test "should get newevent" do
    get :newevent
    assert_response :success
  end

  test "should get editevent" do
    get :editevent
    assert_response :success
  end

  test "should get deleteevent" do
    get :deleteevent
    assert_response :success
  end

  test "should get deleteincharge" do
    get :deleteincharge
    assert_response :success
  end

end
