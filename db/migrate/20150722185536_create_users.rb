class CreateUsers < ActiveRecord::Migration
 def up
    create_table :users do |t|
      t.string "first_name", :limit => 25
      t.string "last_name", :limit => 23
      t.string "username", :limit => 50, :null =>false
      t.string "phone"
      t.string "dept"
      t.string "email", :null => false
      t.integer "permission", :null => false, :limit => 4
      t.string "password_digest"
      t.string :event_name
      t.string :prizeone
      t.string :prizeoneshared
      t.string :prizetwo
      t.string :prizetwoshared
      t.string :prizethree
      t.string :prizethreeshared
      t.string :prizefour
      t.timestamps
    end
  end

  def down
  	drop_table :users
  end
end
