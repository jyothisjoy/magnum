class AlterEvents < ActiveRecord::Migration
  def up
    add_column("events", "prizeone", :string, :limit => 25)
    add_column("events", "prizeoneshared", :string, :limit => 25)
    add_column("events", "prizetwo", :string, :limit => 25)
    add_column("events", "prizetwoshared", :string, :limit => 25)
    add_column("events", "prizethree", :string, :limit => 25)
    add_column("events", "prizethreeshared", :string, :limit => 25)
    add_column("events", "prizefour", :string, :limit => 25)
  end
end
