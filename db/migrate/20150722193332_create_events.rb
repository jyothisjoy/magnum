class CreateEvents < ActiveRecord::Migration
   def up
    create_table :events do |t|
    t.string "title", :null => false, :limit => 100
    t.time "time"
    t.integer "daycount", :default => 1, :null => false
    t.string "dept", :null =>false
    t.string "place", :limit => 50
    t.string "incharge"
    t.string "inchargefb"
    t.string "inchargetwitter"
    t.float "inchargecontact"
    t.integer "prize"
    t.integer "amount", :default => 0
    t.string "eventmode", :default => "technical"
    t.string "about", :limit => 2000
    t.integer "target"
    t.timestamps
    end
  end

  def down
    drop_table :events
  end
end
