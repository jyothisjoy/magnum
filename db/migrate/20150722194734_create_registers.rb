class CreateRegisters < ActiveRecord::Migration
  def change
    create_table :registers do |t|
      t.string :name
      t.string :branch
      t.string :college, :null => false, :limit => 100
      t.integer :sem
      t.string :event1
      t.string :event2
      t.string :event3
      t.string :event4
      t.string :event5
      t.string :event6
      t.string :event7
      t.string :event8
      t.string :event9
      t.string :event10
      t.boolean :ieeemember, :default => false
      t.integer :ieeenum
      t.boolean :acco, :default => false
      t.integer :fee, :default => 0
      t.timestamps
      t.timestamps
     end
  end
end
