class Alter < ActiveRecord::Migration
  def change
   add_column("registers", "groupname1", :string)
   add_column("registers", "groupname2", :string)
   add_column("registers", "groupname3", :string)
   add_column("registers", "phone", :string)
   remove_column("users", "groupname1")
   remove_column("users", "groupname2")
   remove_column("users", "groupname3")
   remove_column("registers", "ieeenum")
   remove_column("registers", "ieeemember")
  end
end
