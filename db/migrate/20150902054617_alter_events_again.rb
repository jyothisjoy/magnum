class AlterEventsAgain < ActiveRecord::Migration
  def change
	    remove_column("events", "prizeone")
	    remove_column("events", "prizeoneshared")
	    remove_column("events", "prizetwo")
	    remove_column("events", "prizetwoshared")
	    remove_column("events", "prizethree")
	    remove_column("events", "prizethreeshared")
	    remove_column("events", "prizefour")
	    add_column("events","visible", :boolean, :default => true)
	    add_column("events", "groupornot", :boolean, :default => false)
  end
end
