# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150902064453) do

  create_table "events", force: true do |t|
    t.string   "title",              limit: 100,                        null: false
    t.datetime "time"
    t.integer  "daycount",                        default: 1,           null: false
    t.string   "dept",                                                  null: false
    t.string   "place",              limit: 50
    t.string   "incharge"
    t.string   "inchargefb"
    t.string   "inchargetwitter"
    t.float    "inchargecontact",    limit: 24
    t.integer  "prize"
    t.integer  "amount",                          default: 0
    t.string   "eventmode",                       default: "technical"
    t.string   "about",              limit: 2000
    t.integer  "target"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "visible",                         default: true
    t.boolean  "groupornot",                      default: false
  end

  create_table "messages", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "message"
  end

  create_table "registers", force: true do |t|
    t.string   "name"
    t.string   "branch"
    t.string   "college",    limit: 100,                 null: false
    t.integer  "sem"
    t.string   "event1"
    t.string   "event2"
    t.string   "event3"
    t.string   "event4"
    t.string   "event5"
    t.string   "event6"
    t.string   "event7"
    t.string   "event8"
    t.string   "event9"
    t.string   "event10"
    t.boolean  "acco",                   default: false
    t.integer  "fee",                    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "groupname1"
    t.string   "groupname2"
    t.string   "groupname3"
    t.string   "phone"
    t.string   "email"
  end

  create_table "users", force: true do |t|
    t.string   "first_name",         limit: 25
    t.string   "last_name",          limit: 23
    t.string   "username",           limit: 50, null: false
    t.string   "phone"
    t.string   "dept"
    t.string   "email",                         null: false
    t.integer  "permission",                    null: false
    t.string   "password_digest"
    t.string   "event_name"
    t.string   "prizeone"
    t.string   "prizeoneshared"
    t.string   "prizetwo"
    t.string   "prizetwoshared"
    t.string   "prizethree"
    t.string   "prizethreeshared"
    t.string   "prizefour"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

end
