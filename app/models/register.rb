class Register < ActiveRecord::Base
	
  def index
    @events = Event.all
  	@register = Register.new
    @register_count = Register.count + 1
  end
  
  def create
  	    # Instantiate a new object using form parameters
    @register = Register.new(register_params)
    # Save the object
    if verify_recaptcha(:model => @register, :message => "Oh! It's error with reCAPTCHA!") && @register.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "register created successfully."
      redirect_to(:controller => 'welcome', :action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      @register_count = Register.count + 1
      render('index')
    end
  end


  private

   def register_params
      # same as using "params[:subject]", except that it:
      # - raises an error if :subject is not present
      # - allows listed attributes to be mass-assigned
      params.require(:register).permit(:name, :branch, :college, :sem, :event1, :event2, :event3, :ieeemember, :ieeenum, :acco, :created_at)
   end
end
