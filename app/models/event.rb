class Event < ActiveRecord::Base

	has_attached_file :image, :styles => {:large => "600x600", :medium => "400x400#>", :thumb => "100x100#>" }, :default_url => "/images/:style/missing.png", :path => ':rails_root/public/img/event/:filename'
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  # Don't need to validate (in most cases):
  #   ids, foreign keys, timestamps, booleans, counters
  validates_presence_of :title
  validates_length_of :title, :maximum => 255
    # validates_presence_of vs. validates_length_of :minimum => 1
    # different error messages: "can't be blank" or "is too short"
    # validates_length_of allows strings with only spaces!

  scope :visible, lambda { where(:visible => true) }
  scope :invisible, lambda { where(:visible => false) }
  scope :sorted, lambda { order("events.daycount ASC") }
  scope :newest_first, lambda { order("events.created_at DESC")}
  scope :search, lambda {|query|
    where(["name LIKE ?", "%#{query}%"])
  }
end
