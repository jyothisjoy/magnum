
require 'csv'
class AdminController < ApplicationController


before_action :confirm_logged_in

  def index
    @events = Event.sorted
    @admins = User.where(:permission => '2')
    @registers = Register.all
    @incharges = User.where(:permission => '1')
    @admindept = session[:adminbranch]
    @inchevename = session[:inchevent]
  end

  def regfull
    @registers = Register.all
  end

  def messages
    @messages = Messages.all
  end

  def newincharge

    @events = Event.all
    @incharge = User.new({:permission => 1, :event_name => @eventname, :dept => @eventdept})
    @incharge_count = User.count + 1
  end
  
  def createincharge
    @events = Event.all
    @eventnum = Event.count
    @eventfind = Event.find(@eventnum)
    @eventdept = @eventfind.dept
    @eventname = @eventfind.title
     # Instantiate a new object using form parameters
    @incharge = User.new(inch_params)
    @incharge.permission = 1
    # Save the object
    if @incharge.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Incharge created successfully."
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      @incharge_count = Event.count + 1
      render('newincharge')

    end
  end

  def newevent
    @event = Event.new
    @event_count = Event.count + 1
  end

  def createevent
    # Instantiate a new object using form parameters
    @event = Event.new(event_params)
    # Save the object
    if @event.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Event created successfully."
      redirect_to(:controller => 'admin', :action => 'newincharge')
    else
      # If save fails, redisplay the form so user can fix problems
      @event_count = Event.count + 1
      render('newevent')
    end    
  end

  def showevent
    @event = Event.find(params[:id])
  end

  def editevent
    @event = Event.find(params[:id])
    @event_count = Event.count
  end

  def updateevent
        # Find an existing object using form parameters
    @event = Event.find(params[:id])
    # Update the object
    if @event.update_attributes(event_params)
      # If update succeeds, redirect to the index action
      flash[:notice] = "Event updated successfully."
      redirect_to(:action => 'showevent', :id => @event.id)
    else
      # If update fails, redisplay the form so user can fix problems
      @event_count = Event.count
      render('editevent')
    end
  end

  def deleteevent
     @event = Event.find(params[:id])
  end

  def destroyevent
    event = Event.find(params[:id]).destroy
    flash[:notice] = "Event '#{Event.name}' destroyed successfully."
    redirect_to(:action => 'index')
  end

  
  def deleteincharge
  end

  def download
  end

  def downfile
    send_file "#{Rails.root}/tmp/spreadsheet/registeration-#{session[:username]}.csv", :type=>"application/csv", :x_sendfile=>true
  end

  def csvgen

       CSV.open("#{Rails.root}/tmp/spreadsheet/registeration-#{session[:username]}.csv", "wb") do |csv|
          csv << Register.attribute_names
          Register.all.each do |reg|
            csv << reg.attributes.values
          end
        end
       redirect_to(:action => 'download')
  end

  def newadmin
    @admin = User.new
    @admin_count = User.count + 1
  end

  def createadmin
        # Instantiate a new object using form parameters
    @admin = User.new(admin_params)
    @admin.permission = 2
    # Save the object
    if @admin.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Admin created successfully."
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      @admin_count = User.count + 1
      render('newadmin')
    end
  end

  def deleteadmin

  end


private
 
  def prize_params
    params.require(:event).permit(:prizeone, :prizeoneshared, :prizetwo, :prizetwoshared, :prizethree, :prizethreeshared, :prizefour)
  end
  def inch_params
    params.require(:incharge).permit(:first_name, :last_name, :username, :email, :dept, :password, :event_name, :created_at)
  end

  def event_params
    params.require(:event).permit(:title, :time, :daycount, :dept, :groupornot, :position, :place, :incharge, :inchargefb, :inchargetwitter, :inchargecontact, :prize, :target, :eventmode, :about, :image, :created_at)
  end
  def admin_params
      params.require(:admin).permit(:first_name, :last_name, :email, :username, :dept, :password, :groupname1, :groupname2, :groupname3, :created_at)
    end



end
