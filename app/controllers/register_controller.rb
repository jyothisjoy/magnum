class RegisterController < ApplicationController

  layout 'register'

  def index
  	@register = Register.new
    @events = Event.all
    @register_count = Register.count + 1
  end
  
  def create
    @register = Register.new
    @events = Event.all
  	    # Instantiate a new object using form parameters
    @register = Register.new(register_params)
    # Save the object
    if verify_recaptcha(:model => @register, :message => "Oh! It's error with reCAPTCHA!") && @register.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "register created successfully."
      redirect_to(:controller => 'register', :action => 'success')
    else
      # If save fails, redisplay the form so user can fix problems
      flash[:notice] = "Unable to Commit"
      @register_count = Register.count + 1
      render('index')
    end
  end

  def success
    
  end


  private

   def register_params
      # same as using "params[:subject]", except that it:
      # - raises an error if :subject is not present
      # - allows listed attributes to be mass-assigned
      params.require(:register).permit(:name, :branch, :college, :sem, :event1, :event2, :event3, :event4, :event5, :event6, :event7, :event8, :event9, :groupname1,  :acco, :created_at)
   end
end
