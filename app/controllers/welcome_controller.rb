class WelcomeController < ApplicationController
  def index
    @events = Event.all
  end

  def newmessage
  	 @message = Messages.new(messg_params)
    # Save the object
    if @message.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Your message has been sent!"
      redirect_to('http://cecmagnum.com')
    else
      # If save fails, redisplay the form so user can fix problems
      flash[:notice] = "Your message not sent!"
      redirect_to('http://cecmagnum.com')
    end    
  end

private
def messg_params
    params.require(:message).permit(:name, :email, :phone, :message)
end  

end